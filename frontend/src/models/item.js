import { Model, deepClone } from './index'
import { Category } from './category'

export const itemDeepClone = options => deepClone(Item, options)

export class Item extends Model {
  static clone = itemDeepClone

  static filter(item, search) {
    return (
      item.pk === parseInt(search) ||
      item.sku.toLowerCase().includes(search.toLowerCase())
    )
  }

  static _name = 'item'
  static _namePlural = 'items'

  constructor(options = {}) {
    super(options)
  }

  get fields() {
    return { ...super.fields, sku: '', category: null }
  }

  /**
   * Category instance
   *
   * @readonly
   * @memberof Item
   */
  get categoryObj() {
    return this.$store.getters['category/getCategory'](this.category)
  }

  /**
   * Avaibility to set category as object
   *
   *
   * @memberof Item
   */
  set categoryObj(value) {
    if (value === null || value === undefined) {
      this.category = null
    } else if (value instanceof Category && value.pk) {
      this.category = value.pk
    } else if (
      this.$store.getters['category/categories'].hasOwnProperty(value)
    ) {
      this.category = value
    }
  }
}
