//API model's primary key
export const PK_KEY = 'id'

// Base model class
export class Model {
  constructor(options = {}) {
    for (const field in this.fields) {
      if (this.fields.hasOwnProperty(field)) {
        const defaultValue = this.fields[field]

        this[field] = options[field] || defaultValue
      }
    }
  }

  get $store() {
    return window.$app.$store
  }

  get pk() {
    return this[PK_KEY]
  }

  get fields() {
    return { [PK_KEY]: null }
  }

  clone() {
    return this.clone(this)
  }

  toObject() {
    let object = {}

    for (const field of Object.keys(this.fields)) {
      object[field] = this[field]
    }

    return object
  }
}

export const deepClone = (model, options) => {
  let clone = new model()

  if (options === null || options === undefined) return clone

  const cloneProperties = Object.assign({}, options)

  for (const key in cloneProperties) {
    if (cloneProperties.hasOwnProperty(key)) {
      const value = cloneProperties[key]

      window.$app.$set(clone, key, value)
    }
  }

  return clone
}
