import { Model, deepClone } from './index'

export const categoryDeepClone = options => deepClone(Category, options)

export class Category extends Model {
  static clone = categoryDeepClone

  static filter(category, search) {
    return (
      category.pk === parseInt(search) ||
      category.name.toLowerCase().includes(search.toLowerCase())
    )
  }

  static _name = 'category'
  static _namePlural = 'categories'

  static parentsHierarchySort(a, b, order) {
    const nameOrder = order ? 1 : 1

    if (a.parentsHierarchyPKs < b.parentsHierarchyPKs) return -1
    if (a.parentsHierarchyPKs > b.parentsHierarchyPKs) return 1

    if (a.name.toLowerCase() < b.name.toLowerCase()) return -nameOrder
    if (a.name.toLowerCase() > b.name.toLowerCase()) return nameOrder

    return 0
  }

  constructor(options = {}) {
    super(options)
  }

  get fields() {
    return { ...super.fields, name: '', parent: null }
  }

  /**
   * Parent category instance
   *
   * @readonly
   * @memberof Category
   */
  get parentObj() {
    return this.$store.getters['category/getCategory'](this.parent)
  }

  /**
   * Avaibility to set parent as object
   *
   *
   * @memberof Category
   */
  set parentObj(value) {
    if (value === null || value === undefined) {
      this.parent = null
    } else if (value instanceof Category && value.pk) {
      this.parent = value.pk
    } else if (
      this.$store.getters['category/categories'].hasOwnProperty(value)
    ) {
      this.parent = value
    }
  }

  /**
   * Array of children categories
   *
   * @readonly
   * @memberof Category
   */
  get children() {
    const arr = []

    for (const category of this.$store.getters[
      'category/categoriesArray'
    ].filter(category => category.parent === this.pk)) {
      arr.push(category)
    }

    return arr
  }

  get hasChildren() {
    return this.children.length
  }

  /**
   * Array of parent categories in root-to-child order
   *
   * @readonly
   * @memberof Category
   */
  get parents() {
    let category = this

    const arr = []

    if (category) {
      while (category.parentObj) {
        arr.push(category.parentObj)

        category = category.parentObj
      }
    }

    return arr.reverse()
  }

  /**
   * Array of !all! children categories
   *
   * @readonly
   * @memberof Category
   */
  get allChildren() {
    const arr = []

    const setChildren = children => {
      for (const child of children) {
        arr.push(child)
        setChildren(child.children)
      }
    }

    setChildren(this.children)

    return arr
  }

  get parentsHierarchyText() {
    const separator = ' → '
    const parentsNames = this.parents.map(category => category.name)
    return `${parentsNames.join(separator)}${
      parentsNames.length ? separator : ''
    }${this.name}`
  }

  get parentsHierarchyPKs() {
    if (this.pk) {
      return [...this.parents.map(c => c.pk), this.pk].join('-')
    }
  }
}
