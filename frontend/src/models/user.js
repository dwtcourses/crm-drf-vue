import { Model, deepClone } from './index'

export const userDeepClone = options => deepClone(User, options)

export class User extends Model {
  static clone = userDeepClone

  static filter(user, search) {
    return (
      user.pk === parseInt(search) ||
      user.phone.toLowerCase().includes(search.toLowerCase()) ||
      user.full_name.includes(search.toLowerCase())
    )
  }

  constructor(options = {}) {
    super(options)
  }

  get fields() {
    return {
      ...super.fields,
      phone: '',
      first_name: '',
      last_name: '',
      last_login: '',
      date_joined: ''
    }
  }

  get full_name() {
    return `${this.last_name} ${this.first_name}`
  }
}
