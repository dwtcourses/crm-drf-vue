import { deepClone } from '@/models'
import { User } from './user'
import { Store } from './store'

export const sellerDeepClone = options => deepClone(Seller, options)

export class Seller extends User {
  static clone = sellerDeepClone

  get fields() {
    return { ...super.fields, store: null }
  }

  static _name = 'seller'
  static _namePlural = 'sellers'

  /**
   * Store instance
   *
   * @readonly
   * @memberof Item
   */
  get storeObj() {
    const store = this.$store.getters['store/storesArray'].filter(
      store => store.seller == this.pk
    )[0]
    return store || null
  }

  // /**
  //  * Avaibility to set store as object
  //  *
  //  * @memberof Item
  //  */
  // set storeObj(value) {
  //   if (value === null || value === undefined) {
  //     this.store = null
  //   } else if (value instanceof Store && value.pk) {
  //     this.store = value.pk
  //   } else if (this.$store.getters['store/stores'].hasOwnProperty(value)) {
  //     this.store = value
  //   }
  // }
}
