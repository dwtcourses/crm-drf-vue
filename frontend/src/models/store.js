import { Model, deepClone } from './index'
import { Seller } from './seller'

export const storeDeepClone = options => deepClone(Store, options)

export class Store extends Model {
  static clone = storeDeepClone

  static filter(store, search) {
    return (
      store.pk === parseInt(search) ||
      store.address.toLowerCase().includes(search.toLowerCase())
    )
  }

  static _name = 'store'
  static _namePlural = 'stores'

  constructor(options = {}) {
    super(options)
  }

  get fields() {
    return { ...super.fields, address: '', seller: null }
  }

  /**
   * Seller instance
   *
   * @readonly
   * @memberof Item
   */
  get sellerObj() {
    return this.$store.getters['seller/getSeller'](this.seller)
  }

  /**
   * Avaibility to set seller as object
   *
   * @memberof Item
   */
  set sellerObj(value) {
    if (value === null || value === undefined) {
      this.seller = null
    } else if (value instanceof Seller && value.pk) {
      this.seller = value.pk
    } else if (this.$store.getters['seller/sellers'].hasOwnProperty(value)) {
      this.seller = value
    }
  }
}
