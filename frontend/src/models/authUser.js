import Cache from '@/utils/cache'
import { User } from './user'

/**
 * This model represents application authentication user
 * Should be initialized only one instance of this class during application runtime
 *
 * @export
 * @class AuthUser
 * @extends {User}
 */
export class AuthUser extends User {
  constructor(options = {}) {
    super(options)
  }

  get token() {
    return Cache.get('token')
  }

  set token(value) {
    Cache.set('token', value)
  }

  get locale() {
    return Cache.get('locale')
  }

  set locale(value) {
    Cache.set('locale', value)
  }

  get is_authenticated() {
    return this.token && this.pk
  }

  login(userData, token = null) {
    if (userData === null) {
      this.logout()
      return
    }
    if (token) Cache.set('token', token)

    for (const field in this.fields) {
      if (this.fields.hasOwnProperty(field)) {
        const defaultValue = this.fields[field]
        this[field] = userData[field] || defaultValue
      }
    }
  }

  logout() {
    Cache.delete('token')

    for (const field in this.fields) {
      if (this.fields.hasOwnProperty(field)) {
        const defaultValue = this.fields[field]
        this[field] = defaultValue
      }
    }
  }

  get language() {
    return this.$store.getters.currentLanguage
  }
}
