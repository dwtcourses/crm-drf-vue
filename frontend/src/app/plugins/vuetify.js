import Vue from 'vue'
import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'
import 'material-icons/iconfont/material-icons.scss'
import { preset } from 'vue-cli-plugin-vuetify-preset-reply/preset'
// Translation provided by Vuetify (typescript)
import ru from 'vuetify/es5/locale/ru'
Vue.use(Vuetify)

export default new Vuetify({
  preset,
  icons: {
    iconfont: 'md'
  },
  lang: {
    locales: { ru }
  }
  // theme: {
  //   dark: false,
  //   themes: {
  //     light: {
  //       primary: '#4f3961',
  //       secondary: '#ea728c',
  //       error: '#FE5F55',
  //       warning: '#FB8B24',
  //       info: '#414141',
  //       success: '#7dd447',
  //       accent: '#fc9d9d'
  //     }
  //   }
  // }
})
