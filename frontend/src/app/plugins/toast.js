import Vue from 'vue'
import VuetifyToast from 'vuetify-toast-snackbar'

Vue.use(VuetifyToast, {
  x: 'center',
  y: 'bottom',
  color: 'dark',
  icon: '',
  iconColor: '',
  classes: ['body-1'],
  timeout: 4000,
  dismissable: true,
  multiLine: false,
  vertical: false,
  queueable: true,
  showClose: true,
  closeText: '',
  closeIcon: 'close',
  closeColor: 'rgba(255,255,255,.3)',
  slot: [],
  shorts: {
    success: {
      color: 'success',
      closeColor: 'rgba(0,0,0,.3)'
    },
    error: {
      color: 'error',
      closeColor: 'rgba(0,0,0,.3)'
    },
    info: {
      color: 'info',
      closeColor: 'rgba(0,0,0,.3)'
    },
    warning: {
      color: 'warning',
      closeColor: 'rgba(0,0,0,.3)'
    }
  },
  property: '$toast'
})
