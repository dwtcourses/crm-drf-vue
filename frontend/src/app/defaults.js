const defaults = {
  fadeTransitionMode: 'out-in',
  fadeTransitionDuration: 100, // milliseconds,
  languages: ['en', 'ru'], // availible interface languages
  phoneNumberMask: '+38(0##) ### ####'
}

Object.freeze(defaults)

export default defaults
