import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import Vuelidate from 'vuelidate'
import VueWait from 'vue-wait'

// Components - autosetting global components, component name format = <pathToFile-fileName>
// F.e "./components/wrappers/PageWrapper.vue" is <wrappers-page-wrapper />
import './components'

// Plugins
import vuetify from './plugins/vuetify'
import './plugins/toast'
import { ApiVuePlugin } from '@/api'
import applicationDefaults from './defaults'

//Vue filters
import './filters'

Vue.use(Vuelidate)
Vue.use(VueWait)
Vue.use(ApiVuePlugin)

const DEBUG = process.env.NODE_ENV === 'development'

Vue.config.productionTip = !DEBUG

// Development logging only
Vue.prototype.$dlog = DEBUG ? console.log : () => {}

//Set app defaults (constansts)
Vue.prototype.$defaults = applicationDefaults

const app = new Vue({
  router,
  store,
  vuetify,
  wait: new VueWait({
    useVuex: true,
    registerComponent: false
  }),
  render: h => h(App)
})

export default app
