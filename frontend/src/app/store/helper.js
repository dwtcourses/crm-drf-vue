import Vue from 'vue'
import { Model, PK_KEY } from '@/models'

const setToStore = (model, state, object) => {
  Vue.set(state, object[PK_KEY], new model(object))
}

/**
 * Map base getters
 *
 * returns {
 *    [model_plural],
 *    [model_plural]Array,
 *    get[model]
 * }
 *
 * For example, model - Category
 * returns {
 *    categories: {primary_key => Category, ...} - object of all categories
 *    categoriesArray: [Category, ...] - array of all categories
 *    getCategory: function (primary_key) - return Category object
 * }
 * @param {Model} model Model class
 */
const mapModelBaseStoreGetters = (model, name, pluralName) => {
  return {
    [pluralName]: state => {
      return state.list
    },
    [`${pluralName}Array`]: state => {
      return Object.values(state.list)
    },
    [`get${name.charAt(0).toUpperCase() + name.slice(1)}`]: state => pk => {
      if (pk && state.list.hasOwnProperty(pk)) {
        return state.list[pk]
      }

      return null
    }
  }
}

/**
 * Map base mutations
 * returns {
 *    SET_[model_uppercase],
 *    SET_[model_plural_uppercase],
 *    DELETE_[model_uppercase]
 * }
 *
 * For example, model - Category
 * returns {
 *    SET_CATEGORIES: function(objects) - updates or creates categories state list
 *    SET_CATEGORY: function(object) - updates or creates item state object
 *    DELETE_CATEGORY: function (primary_key) - deletes item from state
 * }
 * @param {Model} model Model class
 */
const mapModelBaseStoreMutations = (model, name, pluralName) => {
  return {
    [`SET_${name.toUpperCase()}`]: (state, object) => {
      setToStore(model, state.list, object)
    },
    [`SET_${pluralName.toUpperCase()}`]: (state, objects) => {
      for (const index in objects) {
        if (objects.hasOwnProperty(index)) {
          const object = objects[index]
          setToStore(model, state.list, object)
        }
      }
    },
    [`DELETE_${name.toUpperCase()}`]: (state, pk) => {
      Vue.delete(state.list, pk)
    }
  }
}

/**
 * Map base actions, all of them are asynchronous
 * returns {
 *    fetchAll,
 *    fetch,
 *    add,
 *    edit,
 *    detele,
 *    buldDelete: async function(payload) - {payload: [Array] of primary_keys}
 * }
 *
 * @param {Model} model Model class
 * @param api Model's api
 */
const mapModelBaseStoreActions = (model, name, pluralName, api) => {
  return {
    /**
     * Fetching all categories
     *
     * @param {*} { dispatch, commit, state }
     * @param {object} [params={}]
     * @param {boolean} [force=false]
     * @param {boolean} [silent=false]
     * @returns
     */
    async fetchAll(
      { dispatch, commit, state },
      params = {},
      force = false,
      silent = false
    ) {
      //if data has been already fetched do not do request
      if (state.fetched && !force) return

      if (!silent) dispatch('wait/start', `${name}.fetchAll`, { root: true })

      let response

      await api
        .fetchAll(params)
        .then(res => {
          commit(`SET_${pluralName.toUpperCase()}`, res.data)
          response = res
          state.fetched = true
        })
        .finally(() => {
          if (!silent) dispatch('wait/end', `${name}.fetchAll`, { root: true })
        })

      return Promise.resolve(response)
    },
    /**
     * Fetching item data
     * @param {Number} pk Primary key of item to fetch
     */
    async fetch({ dispatch, commit }, pk) {
      dispatch('wait/start', `${name}.fetch.${pk}`, { root: true })

      let response

      await api
        .fetch(pk)
        .then(res => {
          commit(`SET_${name.toUpperCase()}`, res.data)
          response = res
        })
        .finally(() => {
          dispatch('wait/end', `${name}.fetch.${pk}`, { root: true })
        })

      return Promise.resolve(response)
    },
    /**
     * Add new item
     * @param {Object} payload New item data
     */
    async add({ dispatch, commit, rootGetters }, payload) {
      dispatch('wait/start', `${name}.add`, { root: true })

      let response

      await api
        .add(payload)
        .then(res => {
          commit(`SET_${name.toUpperCase()}`, res.data)
          response = res
          dispatch(
            'afterSet',
            rootGetters[
              `${name}/get${name.charAt(0).toUpperCase() + name.slice(1)}`
            ](res.data[PK_KEY])
          )
        })
        .finally(() => {
          dispatch('wait/end', `${name}.add`, { root: true })
        })

      return Promise.resolve(response)
    },
    /**
     * Add new item
     * @param {Object} payload New item data
     */
    async edit({ dispatch, commit, rootGetters }, payload) {
      dispatch('wait/start', `${name}.edit`, { root: true })

      let response

      await api
        .edit(payload[PK_KEY], payload)
        .then(res => {
          commit(`SET_${name.toUpperCase()}`, res.data)
          response = res
          dispatch(
            'afterSet',
            rootGetters[
              `${name}/get${name.charAt(0).toUpperCase() + name.slice(1)}`
            ](res.data[PK_KEY])
          )
        })
        .finally(() => {
          dispatch('wait/end', `${name}.edit`, { root: true })
        })

      return Promise.resolve(response)
    },
    /**
     *  Delete item
     * @param {Number} pk Primary key of item to delete
     */
    async delete({ dispatch, commit }, pk) {
      dispatch('wait/start', `${name}.delete.${pk}`, { root: true })

      await api
        .delete(pk)
        .then(res => {
          commit(`DELETE_${name.toUpperCase()}`, pk)
          dispatch('afterDelete', pk)
        })
        .finally(() => {
          dispatch('wait/end', `${name}.delete.${pk}`, { root: true })
        })
    },
    /**
     * Bulk deletion of items
     *
     * @param {*} { dispatch, commit }
     * @param {*} payload
     */
    async bulkDelete({ dispatch, commit }, payload) {
      dispatch('wait/start', `${name}.bulkDelete`, { root: true })

      await api
        .bulkDelete(payload)
        .then(res => {
          for (const pk of payload) {
            commit(`DELETE_${name.toUpperCase()}`, pk)
            dispatch('afterDelete', pk)
          }
        })
        .finally(() => {
          dispatch('wait/end', `${name}.bulkDelete`, { root: true })
        })
    },

    /**
     * Callack - after delete
     *
     * @param {*} { dispatch, commit }
     * @param {*} pk
     */
    async afterDelete({ dispatch, commit }, pk) {
      // override it here
    },

    /**
     * Callback - after set (add or edit)
     *
     * @param {*} { dispatch, commit }
     * @param {*} instance
     */ async afterSet({ dispatch, commit }, instance) {
      // override it here
    }
  }
}

const mappers = {
  actions: mapModelBaseStoreActions,
  getters: mapModelBaseStoreGetters,
  mutations: mapModelBaseStoreMutations
}

export const mapModelStore = (type, model, api = null) => {
  if (!Object.keys(mappers).includes(type)) {
    console.error(
      `Mapper for ${type} not found. Availiable values are "${Object.keys(
        mappers
      ).join('", "')}"`
    )
    return {}
  }

  if (!(model.prototype instanceof Model)) {
    console.error(`Model param is not children class of ${Model.name}`)
    return {}
  }

  const name = model._name
  const pluralName = model._namePlural

  return mappers[type](model, name, pluralName, api)
}
