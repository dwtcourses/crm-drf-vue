import { mapModelStore } from '../helper'
import { Item } from '@/models/item'
import ItemApi from '@/api/modules/item.api'

export default {
  namespaced: true,
  state: {
    list: {},
    fetched: false
  },
  getters: {
    ...mapModelStore('getters', Item)
  },
  mutations: {
    ...mapModelStore('mutations', Item)
  },
  actions: {
    ...mapModelStore('actions', Item, ItemApi)
  }
}
