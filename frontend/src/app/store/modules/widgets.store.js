export default {
  namespaced: true,
  state: {
    sidebar: null
  },
  getters: {
    getSidebar: state => {
      return state.sidebar
    }
  },
  mutations: {
    setSidebar(state, sidebar) {
      state.sidebar = sidebar
    }
  }
}
