import I18nApi from '@/api/modules/i18n.api.js'
import Vue from 'vue'

/**
 * Django i18n implamantation for Vuex
 */
const i18n = state => {
  const pluralidx = n => {
    if (state.plural) {
      const v = eval(state.plural)
      if (typeof v == 'boolean') {
        return v ? 1 : 0
      } else {
        return v
      }
    }

    return n == 1 ? 0 : 1
  }

  const gettext = msgid => {
    var value = state.catalog[msgid]
    if (typeof value == 'undefined') {
      return msgid
    } else {
      return typeof value == 'string' ? value : value[0]
    }
  }

  const ngettext = (singular, plural, count) => {
    var value = state.catalog[singular]
    if (typeof value == 'undefined') {
      return count == 1 ? singular : plural
    } else {
      return value.constructor === Array
        ? value[pluralidx(state.plural, count)]
        : value
    }
  }

  const gettext_noop = msgid => msgid

  const pgettext = (context, msgid) => {
    var value = gettext(context + '\x04' + msgid)
    if (value.indexOf('\x04') != -1) {
      value = msgid
    }
    return value
  }

  const npgettext = (context, singular, plural, count) => {
    var value = ngettext(
      context + '\x04' + singular,
      context + '\x04' + plural,
      count
    )
    if (value.indexOf('\x04') != -1) {
      value = ngettext(singular, plural, count)
    }
    return value
  }

  const interpolate = (fmt, obj, named) => {
    if (named) {
      return fmt.replace(/%\(\w+\)s/g, function(match) {
        return String(obj[match.slice(2, -2)])
      })
    } else {
      return fmt.replace(/%s/g, function(match) {
        return String(obj.shift())
      })
    }
  }

  const get_format = format_type => {
    var value = state.formats[format_type]
    if (typeof value == 'undefined') {
      return format_type
    } else {
      return value
    }
  }

  return {
    pluralidx,
    gettext,
    ngettext,
    gettext_noop,
    pgettext,
    npgettext,
    interpolate,
    get_format
    //aliasiss
  }
}

export default {
  namespaced: true,
  state: {
    formats: {},
    catalog: {},
    plural: null,
    locale: ''
  },
  getters: {
    _: state => i18n(state).gettext,
    i18n
  },
  mutations: {
    SET_LOCALE: (state, i18n) => {
      for (const key in i18n) {
        if (i18n.hasOwnProperty(key)) {
          const data = i18n[key]
          Vue.set(state, key, data)
        }
      }
    }
  },
  actions: {
    async fetchLocale({ dispatch, commit, rootGetters }, locale) {
      dispatch('wait/start', `fetch.locale.${locale}`, { root: true })

      I18nApi.fetchLocale(locale)
        .then(res => {
          rootGetters.authUser.locale = locale
          commit('SET_LOCALE', res.data)
        })
        .finally(() => {
          dispatch('wait/end', `fetch.locale.${locale}`, { root: true })
        })
    },
    setLocale({ commit }, i18n) {
      commit('SET_LOCALE', i18n)
    }
  }
}
