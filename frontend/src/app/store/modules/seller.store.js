import { mapModelStore } from '../helper'
import { Seller } from '@/models/seller'
import SellerApi from '@/api/modules/seller.api'

export default {
  namespaced: true,
  state: {
    list: {},
    fetched: false
  },
  getters: {
    ...mapModelStore('getters', Seller)
  },
  mutations: {
    ...mapModelStore('mutations', Seller)
  },
  actions: {
    ...mapModelStore('actions', Seller, SellerApi)
  }
}
