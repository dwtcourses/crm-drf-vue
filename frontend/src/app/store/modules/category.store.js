import { mapModelStore } from '../helper'
import { Category } from '@/models/category'
import CategoryApi from '@/api/modules/categories.api'

export default {
  namespaced: true,
  state: {
    list: {},
    fetched: false
  },
  getters: {
    ...mapModelStore('getters', Category),

    rootCategories: (state, getters) => {
      return getters.categoriesArray.filter(cat => cat.parent === null)
    }
  },
  mutations: {
    ...mapModelStore('mutations', Category),
    AFTER_DELETE: (state, pk) => {
      for (const category of Object.values(state.list).filter(
        category => category.parent == pk
      )) {
        if (state.list.hasOwnProperty(category[PK_KEY])) {
          const category = state.list[category[PK_KEY]]
          category.parent = null
        }
      }
    }
  },
  actions: {
    ...mapModelStore('actions', Category, CategoryApi),

    afterDelete({ commit }, pk) {
      commit('AFTER_DELETE', pk)
    }
  }
}
