import { mapModelStore } from '../helper'
import { Store } from '@/models/store'
import StoreApi from '@/api/modules/store.api'

export default {
  namespaced: true,
  state: {
    list: {},
    fetched: false
  },
  getters: {
    ...mapModelStore('getters', Store)
  },
  mutations: {
    ...mapModelStore('mutations', Store)
  },
  actions: {
    ...mapModelStore('actions', Store, StoreApi)
  }
}
