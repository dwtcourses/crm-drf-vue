import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import modules from './modules'
import { AuthUser } from '@/models/authUser'
import BaseApi from '@/api/modules/base.api'

Vue.use(Vuex)

export default new Vuex.Store({
  modules,
  strict: false,
  state: {
    authUser: new AuthUser(),
    inputsData: {}
  },
  getters: {
    authUser: state => state.authUser,
    inputsData: state => state.inputsData
  },
  mutations: {
    SET_INPUTS_DATA: (state, payload) => {
      for (const key in payload) {
        if (payload.hasOwnProperty(key)) {
          const value = payload[key]
          Vue.set(state.inputsData, key, value)
        }
      }
    }
  },
  actions: {
    async initial({ dispatch, commit, rootGetters }) {
      dispatch('wait/start', 'inital', { root: true })

      await BaseApi.initial()
        .then(({ data: { i18n, user, inputsData, locale } }) => {
          dispatch('i18n/setLocale', i18n)
          commit('SET_INPUTS_DATA', inputsData)

          rootGetters.authUser.locale = locale
          rootGetters.authUser.login(user)
        })
        .finally(() => {
          dispatch('wait/end', 'inital', { root: true })
        })
    },
    async login({ dispatch, rootGetters }, credentials) {
      dispatch('wait/start', 'authUser.login', { root: true })

      await BaseApi.login(credentials)
        .then(({ data: { user, token } }) => {
          rootGetters.authUser.login(user, token)
        })
        .finally(() => {
          dispatch('wait/end', 'authUser.login', { root: true })
        })
    },
    logout({ rootGetters }) {
      rootGetters.authUser.logout()
    }
  }
})
