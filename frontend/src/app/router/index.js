import Vue from 'vue'
import VueRouter from 'vue-router'
import { mapModelsRoutes } from '@/api'
import { Store } from '@/models/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'account',
    redirect: { name: Store._namePlural }
  },
  {
    path: '/login/',
    name: 'login',
    meta: {
      layout: 'authorization',
      authorization: 'required not'
    },
    pathToRegexpOptions: { strict: true },
    component: () => import('../views/Login')
  },

  ...mapModelsRoutes()
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
