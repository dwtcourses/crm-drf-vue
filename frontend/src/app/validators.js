import { helpers } from 'vuelidate/lib/validators'

export const foreign = (items, exclude, allowNull = true) => value => {
  if (allowNull && value === null) return true
  if (exclude instanceof Function) {
    return items
      .filter(i => !exclude(i))
      .map(i => i.pk)
      .includes(value)
  } else {
    return items.map(i => i.pk).includes(value)
  }
}

export const phone = helpers.regex(
  'phone',
  /^(\+38\(0\d{2}\)\s\d{3}\s\d{4})|(380\d{9})$/
)
