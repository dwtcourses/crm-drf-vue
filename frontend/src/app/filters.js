import Vue from 'vue'
// import { sprintf } from 'sprintf-js'

// Vue.filter('f', sprintf)
Vue.filter('duration', function(value) {
  let val = parseInt(value)

  const neg = val < 0

  if (neg) val *= -1

  let hours = (val - (val % 3600)) / 3600
  hours = hours > 0 && hours < 10 ? `0${hours}` : hours

  const mins_left = val - hours * 3600

  let minutes = (mins_left - (mins_left % 60)) / 60
  minutes = minutes > 0 && minutes < 10 ? `0${minutes}` : minutes || '00'

  let seconds = mins_left % 60
  seconds = seconds > 0 && seconds < 10 ? `0${seconds}` : seconds || '00'

  return `${neg ? '-' : ''}${hours ? `${hours}:` : ''}${minutes}:${seconds}`
})

function capitalize(value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
}

Vue.filter('capitalize', capitalize)
Vue.filter('c', capitalize)
