import App from './app'

// Set
Object.defineProperty(window, '$app', {
  value: App
})

window.$app.$mount('#app')
