import http from './http'
import { Model } from '@/models'
import { Store } from '@/models/store'
import { Item } from '@/models/item'
import { Category } from '@/models/category'
import { Seller } from '@/models/seller'

export const LOCALE_DOMAIN = 'frontend'

export const MODELS = [Category, Item, Seller, Store]

export const mapModelsRoutes = () => {
  let result = []

  for (const model of MODELS) {
    const route = model._namePlural

    result.push({
      path: `/${route}/`,
      name: route,
      meta: {
        layout: 'account',
        authorization: 'required',
        model
      },
      pathToRegexpOptions: { strict: true },
      component: () => import(`@/app/views/${route}/List.vue`)
    })

    result.push({
      path: `/${route}/add/`,
      name: `${route}-add`,
      meta: {
        layout: 'account',
        authorization: 'required',
        model
      },
      pathToRegexpOptions: { strict: true },
      component: () => import(`@/app/views/${route}/Add.vue`)
    })

    result.push({
      path: `/${route}/:pk/edit/`,
      name: `${route}-edit`,
      meta: {
        layout: 'account',
        authorization: 'required',
        model
      },
      pathToRegexpOptions: { strict: true },
      component: () => import(`@/app/views/${route}/Edit.vue`)
    })
  }

  return result
}

export const mapApiRoutes = endpoint => ({
  add: payload => {
    if (payload instanceof Model) payload = payload.toObject()

    return http.post(`${endpoint}/`, payload)
  },
  edit: (pk, payload) => {
    if (payload instanceof Model) payload = payload.toObject()

    return http.put(`${endpoint}/${pk}/`, payload)
  },
  fetchAll: (params = {}) => http.get(`${endpoint}/`, { params }),
  fetch: pk => http.get(`${endpoint}/${pk}/`),
  delete: pk => http.delete(`${endpoint}/${pk}/`),
  bulkDelete: payload => http.post(`${endpoint}-delete/`, payload)
})

//Api support plugin for Vue
export const ApiVuePlugin = {
  install: (Vue, options) => {
    //
  }
}
