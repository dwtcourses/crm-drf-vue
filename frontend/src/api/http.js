import Axios from 'axios'
import Cache from '@/utils/cache'

//Axios http client
const http = Axios.create({
  baseURL: process.env.VUE_APP_API_ENDPOINT,
  timeout: 0,
  headers: {
    'Content-Type': 'application/json'
  }
})

//Request interceptor for auth
const authInterceptor = config => {
  const authUser = window.$app.$store.getters.authUser

  if (authUser.token) {
    config.headers['Authorization'] = `Token ${authUser.token}`
  }

  if (authUser.locale) {
    config.headers['Locale'] = authUser.locale
  }

  return config
}

//Response interceptor for errors
const errorInterceptor = error => {
  switch (error.response.status) {
    case 400:
      break
    case 401:
      window.$app.$store.dispatch('logout', { vm: window.$app })
      setTimeout(() => {
        window.$app.$toast.error(window.$app.$store.getters.t.errors['401'])
      }, 50)
      break

    case 500:
      window.$app.$toast.error(window.$app.$store.getters.t.errors['500'])
      break

    default:
      console.error(error.response.status, error.message)
      window.$app.$toast.error(window.$app.$store.getters.t.errors.default)
  }

  return Promise.reject(error)
}

//Response interceptor
const responseInterceptor = response => {
  switch (response.status) {
    case 200:
      break
    default:
  }

  return response
}

http.interceptors.request.use(authInterceptor)
http.interceptors.response.use(responseInterceptor, errorInterceptor)

export default http
