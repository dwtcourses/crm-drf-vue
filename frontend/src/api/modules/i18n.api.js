import http from '../http'
import { mapApiRoutes } from '../index'

export default {
  async fetchLocale(locale) {
    return http.get(`locale/${locale}.json`, {
      baseURL: process.env.VUE_APP_FRONTEND_ENDPOINT
    })
  }
}
