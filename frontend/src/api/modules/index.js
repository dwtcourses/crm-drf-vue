/**
 * Automatically imports all the modules and exports as a single module object
 * Naming format: {camelCasedRouteName}.api.js
 */
const requireModule = require.context('.', false, /\.api\.js$/)
const modules = {}

requireModule.keys().forEach(filename => {
  const moduleName = filename.replace(/(\.\/|\.api\.js)/g, '')

  modules[moduleName] =
    requireModule(filename).default || requireModule(filename)
})

export default modules
