import axios from 'axios'
import http from '../http'
import Cache from '@/utils/cache'
import { LOCALE_DOMAIN } from '../index'

export default {
  initial: () =>
    http.get('initial/', {
      baseURL: process.env.VUE_APP_FRONTEND_ENDPOINT
    }),

  login: async credentials => http.post('login/', credentials)
}
