import http from '../http'
import { mapApiRoutes } from '../index'

export default {
  ...mapApiRoutes('sellers')
}
