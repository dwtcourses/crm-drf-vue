export default {
  get(key, def = null) {
    try {
      if (this.has(key)) return JSON.parse(localStorage.getItem(key))
    } catch {}

    return def
  },
  set(key, value) {
    localStorage.setItem(key, JSON.stringify(value === undefined ? null : value))
  },
  has(key) {
    return localStorage.getItem(key) !== null
  },
  delete(key) {
    localStorage.removeItem(key)
  }
}
