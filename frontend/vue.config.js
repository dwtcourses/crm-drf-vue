module.exports = {
  // publicPath: process.env.NODE_ENV === 'production' ? '/crm/' : '/',
  configureWebpack: {
    optimization: {
      splitChunks: false
    },
    watch: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  }
  // css: {
  //   loaderOptions: {
  //     sass: {
  //       data: `@import "~@/scss/variables.scss"` // change the route with you main.scss location in yout proyect
  //     }
  //   }
  // },
  // chainWebpack: config => {
  //   ;['vue-modules', 'vue', 'normal-modules', 'normal'].forEach(match => {
  //     config.module
  //       .rule('scss')
  //       .oneOf(match)
  //       .use('sass-loader')
  //       .tap(opt =>
  //         Object.assign(opt, { data: `@import '~@/scss/variables.scss';` })
  //       )
  //   })
  // }
}
