from django.db import models
from django.utils.translation import ugettext_lazy as _

from .base import BaseModel, BaseModelSerializerMeta
from rest_framework import serializers


class Store(BaseModel):
    address = models.CharField(
        max_length=255, verbose_name=_('address'), blank=True)
    seller = models.OneToOneField(
        'User', null=True, on_delete=models.SET_NULL, verbose_name=_('seller'), related_name='store')

    serializer_class = __name__ + '.StoreSerializer'

    class Meta:
        verbose_name = _('store')
        verbose_name_plural = _('stores')


class StoreSerializer(serializers.ModelSerializer):

    class Meta(BaseModelSerializerMeta):
        model = Store
