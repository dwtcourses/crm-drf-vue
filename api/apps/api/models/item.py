from django.db import models
from django.utils.translation import ugettext_lazy as _

from .base import BaseModel, BaseModelSerializerMeta
from rest_framework import serializers


class Item(BaseModel):
    SKU_CHARS_LIMIT = 6

    sku = models.CharField(max_length=50, verbose_name=_(
        'sku'), db_index=True)

    category = models.ForeignKey(
        'Category', on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_(
            'category'))

    serializer_class = __name__ + '.ItemSerializer'

    @classmethod
    def generate_sku(cls, instance):
        if instance.pk:
            length = len(str(instance.pk))

            if (cls.SKU_CHARS_LIMIT - length) > 0:
                sku = "".join(
                    ["0" for _ in range(0, cls.SKU_CHARS_LIMIT - length)]) + str(instance.pk)
            else:
                sku = str(instance.pk)

            return sku

    class Meta:
        verbose_name = _('item')
        verbose_name_plural = _('items')


class ItemSerializer(serializers.ModelSerializer):
    sku = serializers.CharField(read_only=True)

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     user = self.context['request'].user
    #     self.fields['category'].queryset = user.categories

    class Meta:
        model = Item
        fields = ['id', 'sku', 'category']
