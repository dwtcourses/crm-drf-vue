import inspect
import importlib
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from django.utils.functional import cached_property
from django.utils.decorators import classproperty


class BaseManager(models.Manager):
    def enabled(self):
        return self.filter(_active=True)


class BaseModel(models.Model):
    objects = BaseManager()

    _active = models.BooleanField(default=True)

    @classproperty
    def pk_key(cls):
        return cls._meta.pk.name

    serializer_class = serializers.ModelSerializer

    @property
    def serializer(self):
        return self.get_serializer_class()(self)

    @classmethod
    def get_serializer_class(cls, *args, **kwargs):
        if inspect.isclass(cls.serializer_class):
            return cls.serializer_class
        elif isinstance(cls.serializer_class, str):
            module_name = '.'.join(cls.serializer_class.split('.')[0:-1])
            class_name = cls.serializer_class.split('.')[-1]

            module = importlib.import_module(module_name)
            _class = getattr(module, class_name)

            return _class

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class BaseModelSerializerMeta:
    exclude = ['_active']
