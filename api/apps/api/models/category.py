from __future__ import annotations

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.functional import cached_property
from rest_framework import serializers

from .base import BaseModel, BaseModelSerializerMeta


class Category(BaseModel):
    name = models.CharField(max_length=50, verbose_name=_('name'))
    parent = models.ForeignKey(
        'self', on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_(
            'parent category'))

    @cached_property
    def parents(self):
        parents = []

        category = self

        while category.parent:
            parents.append(category.parent)
            category = category.parent

        return parents

    def is_child(self, category: Category) -> bool:
        if category in self.parents:
            return True

        return False

    serializer_class = __name__ + '.CategorySerializer'

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')


class CategorySerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Category
