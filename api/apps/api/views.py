import importlib
import os

from django.conf import settings
from django.http import HttpResponseNotFound
from django.utils.translation import activate, get_language
from django.utils.translation import gettext as _
from django.views.i18n import (DjangoTranslation, JavaScriptCatalog,
                               JSONCatalog, get_formats)
from rest_framework import authentication, exceptions, permissions
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User

# from .components.frontend_data import TranslationsData, ValidationsData


class Login(ObtainAuthToken):
    authentication_classes = []
    permission_classes = [permissions.AllowAny]

    def post(self, request, *args, **kwargs):
        request.data.update(
            {'username': User.normalize_username(request.data.get('username', ''))})

        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user': user.serializer.data
        })


class JSONI18n(JSONCatalog):
    @classmethod
    def check_if_exists(cls, domain, locale):
        if locale not in [lang for lang, _ in settings.LANGUAGES]:
            return False

        # for translation in list(filter(lambda t: t['DOMAIN'] == domain, settings.APPS['api']['TRANSLATIONS'])):
        #     if os.path.exists(os.path.join(translation['PATH'], locale,  'LC_MESSAGES', '%s.mo' % translation['DOMAIN'])):
        #         return True

        return True

    def get_context_data(self, **kwargs):
        if not self.check_if_exists(locale=kwargs.get('locale'), domain=kwargs.get('domain')):
            raise Exception(_('Doenst exist'))

        packages = [kwargs.get('app')] if kwargs.get('app') else None
        self.translation = DjangoTranslation(
            kwargs.get('locale'), domain=kwargs.get('domain'), localedirs=self.get_paths(packages))

        context = super().get_context_data(**kwargs)
        context['catalog'].update(self.extra(**kwargs))

        return context

    def get(self, request, **kwargs):
        kwargs.setdefault('domain', 'i18n')

        try:
            context = self.get_context_data(**kwargs)
        except Exception as e:
            return HttpResponseNotFound()

        return self.render_to_response(context)

    @classmethod
    def extra(cls, **kwargs):
        app = kwargs.get('app')

        result = {}
        current_lang = get_language()
        activate(kwargs.get('locale'))

        try:
            module = importlib.import_module(app + '.i18n')
        except ModuleNotFoundError as e:
            return result

        app_path = os.path.dirname(module.__file__)

        entries = getattr(module, 'DYNAMIC_ENTRIES', None)

        def set_entry(key, value):
            if isinstance(value, dict):
                for _key, _value in value.items():
                    set_entry('{}.{}'.format(key, _key), _value)
            else:
                result[key] = value

        if entries:
            for key, value in entries.items():
                set_entry(key, value)

        return result
