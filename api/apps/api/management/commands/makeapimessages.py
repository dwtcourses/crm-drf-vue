import polib
import os
import importlib
import re
import django

from datetime import datetime

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.conf import settings
from django.utils.timezone import now

from apps.api.models import Category, User, Item


plural_forms_re = re.compile(
    r'^"Plural-Forms:(?P<value>.+?)\\n"\s*$', re.MULTILINE | re.DOTALL)


class Command(BaseCommand):
    def handle(self, *args, **options):
        for app in settings.I18N_APPS:
            try:
                module = importlib.import_module(app + '.i18n')
            except ModuleNotFoundError as e:
                self.stdout.write(str(e))
                continue

            app_path = os.path.dirname(module.__file__)

            entries = getattr(module, 'ENTRIES', None)

            if not entries:
                self.stdout.write(
                    'Package i18n was found in {app} application, but ENTRIES is missing'.format(app))
                continue

            for lang_code in settings.LANGUAGE_CODES:
                locale_path = os.path.join(
                    app_path, 'locale', lang_code, 'LC_MESSAGES')
                if not os.path.exists(locale_path):
                    os.makedirs(locale_path)

                pofile_path = os.path.join(locale_path, 'i18n.po')

                pofile = polib.POFile(check_for_duplicates=True)

                existing_pofile = None

                if(os.path.exists(pofile_path)):
                    existing_pofile = polib.pofile(
                        pofile_path)
                    pofile.metadata = existing_pofile.metadata

                else:
                    pofile.metadata = {
                        'POT-Creation-Date': self.now(),
                        'MIME-Version':  '1.0',
                        'Content-Type': 'text/plain; charset=UTF-8',
                        'Content-Transfer-Encoding': '8bit'
                    }

                plural_forms = self.get_plural_forms(lang_code)

                pofile.metadata.update({
                    'PO-Revision-Date': self.now(),
                    'Plural-Forms': plural_forms.replace('"\n"', '').strip() if plural_forms else 'nplurals=2; plural=(n != 1);'
                })

                def set_entry(key, value):
                    if isinstance(value, dict):
                        for _key, _value in value.items():
                            set_entry('{}.{}'.format(key, _key), _value)
                    elif isinstance(value, str):
                        POEntry = polib.POEntry(
                            msgid=key, msgstr=(self.get_value(key, existing_pofile)))
                        pofile.append(POEntry)
                    elif isinstance(value, list):
                        for _value in value:
                            _key = '{}.{}'.format(key, _value)
                            POEntry = polib.POEntry(
                                msgid=_key, msgstr=(self.get_value(_key, existing_pofile)))
                            pofile.append(POEntry)

                if entries:
                    for key, value in entries.items():
                        set_entry(key, value)

                pofile.save(pofile_path)

    @classmethod
    def now(cls):
        return datetime.now()

    @classmethod
    def get_plural_forms(cls, locale):
        """
        Copy plural forms header contents from a Django catalog of locale to
        the msgs string, inserting it at the right place. msgs should be the
        contents of a newly created .po file.
        """
        django_dir = os.path.normpath(
            os.path.join(os.path.dirname(django.__file__)))
        domains = ('django',)
        for domain in domains:
            django_po = os.path.join(
                django_dir, 'conf', 'locale', locale, 'LC_MESSAGES', '%s.po' % domain)
            if os.path.exists(django_po):
                with open(django_po, encoding='utf-8') as fp:
                    m = plural_forms_re.search(fp.read())
                if m:
                    return m.group('value')

    @classmethod
    def get_value(cls, key: str, existing_pofile: polib.POFile) -> str:
        return existing_pofile.find(key).msgstr if existing_pofile and existing_pofile.find(key) else ""
