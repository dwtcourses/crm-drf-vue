from django.core.management.base import BaseCommand, CommandError
from apps.api.models import Category, User, Item
import polib
import os
from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):
        print(Category._meta.get_field('parent').verbose_name)
