import polib
import os
import importlib
import re
import django

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError

from django.conf import settings

from apps.api.models import Category, User, Item

plural_forms_re = re.compile(
    r'^(?P<value>"Plural-Forms.+?\\n")\s*$', re.MULTILINE | re.DOTALL)


class Command(BaseCommand):
    def handle(self, *args, **options):
        for lang_code, _ in settings.LANGUAGES:

            for translation in settings.APPS['api']['TRANSLATIONS']:
                entries = getattr(importlib.import_module(
                    translation['ENTRIES']), 'ENTRIES', [])

                path = os.path.join(
                    translation['PATH'], lang_code,  'LC_MESSAGES', '%s.po' % translation['DOMAIN'])

                pofile = polib.POFile(check_for_duplicates=True)

                if(os.path.exists(path)):
                    existing_pofile = polib.pofile(
                        path)
                else:
                    os.makedirs(os.path.dirname(path))
                    existing_pofile = None

                for key in entries:
                    entry = polib.POEntry(
                        msgid=self.get_key(key), msgstr=self.get_value(key, existing_pofile))
                    pofile.append(entry)

                print(pofile)

                print(self.copy_plural_forms(str(pofile), lang_code))

                pofile.save(path)

    def copy_plural_forms(self, msgs, locale):
        """
        Copy plural forms header contents from a Django catalog of locale to
        the msgs string, inserting it at the right place. msgs should be the
        contents of a newly created .po file.
        """
        django_dir = os.path.normpath(
            os.path.join(os.path.dirname(django.__file__)))
        domains = ('django',)
        for domain in domains:
            django_po = os.path.join(
                django_dir, 'conf', 'locale', locale, 'LC_MESSAGES', '%s.po' % domain)
            if os.path.exists(django_po):
                with open(django_po, encoding='utf-8') as fp:
                    m = plural_forms_re.search(fp.read())
                if m:
                    plural_form_line = m.group('value')
                    lines = []
                    found = False
                    for line in msgs.splitlines():
                        if not found and (not line or plural_forms_re.search(line)):
                            line = plural_form_line
                            found = True
                        lines.append(line)
                    msgs = '\n'.join(lines)
                    break
        return msgs

    @classmethod
    def get_value(cls, key, existing_pofile):
        value = None

        try:
            if isinstance(key, str):
                if existing_pofile and existing_pofile.find(key):
                    value = existing_pofile.find(key).msgstr
            elif isinstance(key, tuple):
                _key, default_value = key
                value = cls.get_value(_key, existing_pofile) or default_value

        except Exception as err:
            print(err)

        return value or ""

    @classmethod
    def get_key(cls, key):
        if isinstance(key, tuple):
            _key, _ = key
            return _key

        return key
