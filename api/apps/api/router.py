from rest_framework.routers import SimpleRouter, Route


class Router(SimpleRouter):
    routes = SimpleRouter.routes + [
        # Bulk deletion route
        Route(url=r'^{prefix}-delete{trailing_slash}$',
              mapping={'post': 'bulk_destroy'},
              name='{basename}-list',
              detail=False,
              initkwargs={'suffix': 'List'}),
    ]
