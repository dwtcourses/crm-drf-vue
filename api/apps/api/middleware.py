import pprint
from django.conf import settings
from django.utils.translation import activate


class LocaleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        language = request.headers.get('Locale')

        if language and language in settings.LANGUAGE_CODES:
            activate(language)

        response = self.get_response(request)
        return response
