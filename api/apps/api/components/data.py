from django.utils.translation import ugettext_lazy as _

from ..models import Category, Item, Store, User


class Data:
    def __init__(self):
        self.data = {}

        for attribute, value in self.__class__.__dict__.items():
            if not callable(getattr(self.__class__, attribute)) and not attribute.startswith('__') and not attribute.endswith('__'):
                self.data[attribute] = value

    def __iter__(self, *args, **kwargs):
        for k, v in self.data.items():
            yield k, v

    def __getitem__(self, *args, **kwargs):
        return self.data.__getitem__(*args, **kwargs)


class InputsData(Data):
    login = {
        'username': {
            'required': True,
            'maxLength': User._meta.get_field(User.USERNAME_FIELD).max_length,
        },
        'password': {
            'required': True,
            'maxLength': User._meta.get_field('password').max_length,
        },
    }

    seller = {
        'phone': {
            'required': True,
            'maxLength': User._meta.get_field('phone').max_length,
        },
        'first_name': {
            'required': True,
            'maxLength': User._meta.get_field('first_name').max_length,
        },
        'last_name': {
            'required': True,
            'maxLength': User._meta.get_field('last_name').max_length,
        },
        'store': {
            'required': True
        }
    }

    category = {
        'name': {
            'required': True,
            'maxLength': Category._meta.get_field('name').max_length,
        },
        'parent': {
            'required': False
        }
    }

    item = {
        'category': {
            'required': True
        }
    }

    store = {
        'address': {
            'maxLength': Store._meta.get_field('address').max_length,
            'required': True
        },
        'seller': {
            'required': False
        }
    }

    # expense = {
    #     'name': {
    #         'label': Expense._meta.get_field('name').verbose_name,
    #         'maxLength': Expense._meta.get_field('name').max_length,
    #         'error': {},
    #     }
    # }

    # income = {
    #     'name': {
    #         'label': Income._meta.get_field('name').verbose_name,
    #         'maxLength': Income._meta.get_field('name').max_length,
    #         'error': {},
    #     }
    # }
