import re

from django.core.validators import RegexValidator


def phone_validator(value):
    value = ''.join(re.findall(r'\d+', value))
    return RegexValidator(
        regex=r'^380\d{9}$')(value)
