"""crm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from apps.api.views import JSONI18n
from django.contrib import admin
from django.urls import path, include
from .views import Login

from . router import Router
from . import viewsets

router = Router()

router.register(r'categories', viewsets.CategoriesViewset,
                basename='categories')
router.register(r'items', viewsets.ItemsViewset, basename='items')
router.register(r'sellers', viewsets.SellersViewset, basename='sellers')
router.register(r'stores', viewsets.StoresViewset, basename='stores')

urlpatterns = [
    path('login/', Login.as_view()),
    path('', include(router.urls)),
]
