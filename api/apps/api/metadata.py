from pprint import pformat

from rest_framework.serializers import PrimaryKeyRelatedField
from rest_framework.metadata import SimpleMetadata
from rest_framework.request import clone_request


class Metadata(SimpleMetadata):
    pass
