from rest_framework import viewsets

from ..models import Item

from .base import BaseModelViewSet


class ItemsViewset(BaseModelViewSet):
    serializer_class = Item.get_serializer_class()

    def get_queryset(self):
        return Item.objects.enabled()

    def perform_create(self, serializer):
        item = serializer.save()
        item.sku = Item.generate_sku(item)
        item.save()
