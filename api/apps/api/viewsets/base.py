import time

from django.core.exceptions import ObjectDoesNotExist

from rest_framework import viewsets, response, status, permissions

from ..permissions import IsAdminOrReadOnly


class BaseModelViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated, IsAdminOrReadOnly]

    def perform_destroy(self, instance):
        instance._active = False
        instance.save()

    def list(self, request, *args, **kwargs):
        time.sleep(1)
        return super().list(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        time.sleep(1)
        return super().destroy(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        time.sleep(1)
        return super().update(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        time.sleep(1)
        return super().create(request, *args, **kwargs)

    def bulk_destroy(self, request, *args, **kwargs):
        for pk in request.data:
            try:
                int(pk)
                instance = self.get_queryset().get(pk=pk)
                self.perform_destroy(instance)
            except ValueError:
                pass
            except ObjectDoesNotExist:
                pass

        return response.Response(status=status.HTTP_204_NO_CONTENT)
