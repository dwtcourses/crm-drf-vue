from rest_framework import viewsets

from ..models import Store


class StoresViewset(viewsets.ModelViewSet):
    serializer_class = Store.get_serializer_class()

    def get_queryset(self):
        return Store.objects.enabled()
