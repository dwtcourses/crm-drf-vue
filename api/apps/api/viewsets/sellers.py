from django.db.models import Q

from rest_framework import viewsets

from ..models import User


class SellersViewset(viewsets.ModelViewSet):
    serializer_class = User.get_serializer_class()

    def get_queryset(self):
        return User.objects.enabled().filter(Q(is_staff=False) & Q(is_superuser=False))
