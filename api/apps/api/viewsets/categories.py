import time

from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

from rest_framework import filters, response, status, exceptions

from ..models import Category
from .base import BaseModelViewSet


class CategoriesViewset(BaseModelViewSet):
    def get_serializer_class(self):
        return Category.get_serializer_class()

    def get_queryset(self):
        return Category.objects.enabled()

    def perform_destroy(self, instance):
        Category.objects.filter(parent=instance).update(parent=None)
        super().perform_destroy(instance)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        parent_pk = request.data.get('parent')

        # cheking if parent category is parent category of instance
        if parent_pk:
            try:
                new_parent = request.user.categories.get(pk=parent_pk)
                if new_parent.is_child(instance):
                    raise exceptions.ValidationError(
                        {'parent': _('Set child category as parent not allowed')})

            except ObjectDoesNotExist:
                raise exceptions.ValidationError(
                    {'parent': _('Parent category value in invalid')})

        return super().update(request, *args, **kwargs)
