from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from apps.api.models import Category, Item, Store, User

ENTRIES = {
    'language': '',
    **{'language.{}'.format(lang_code): '' for lang_code in settings.LANGUAGE_CODES},

    'actions': ['login', 'logout', 'edit', 'add', 'delete', 'search', 'listActions', 'save'],

    'dashboard': {
        'menu': '',
        'heading': '',
    },

    'login': [
        'heading',
        'submit'
    ],

    'category': {
        'menu': '',
        'heading': [
            'list',
            'add',
            'edit',
            'view',
        ],
        'list': [
            'delete',
            'add',
            'deleted',
            'bulkDeleted',
            'confirmDelete',
            'confirmBulkDelete',
            'edited',
            'added',
            'notFound',
            'noRecords',
            'deselect',
            'expand',
            'close'
        ]
    },

    'item': {
        'menu': '',
        'heading': [
            'list',
            'add',
            'edit',
            'view',
        ],
        'list': [
            'delete',
            'add',
            'deleted',
            'bulkDeleted',
            'confirmDelete',
            'confirmBulkDelete',
            'edited',
            'added',
            'notFound',
            'noRecords',
        ]
    },

    'store': {
        'menu': '',
        'heading': [
            'list',
            'add',
            'edit',
            'view',
        ],
        'list': [
            'delete',
            'add',
            'deleted',
            'bulkDeleted',
            'confirmDelete',
            'confirmBulkDelete',
            'edited',
            'added',
            'notFound',
            'noRecords',
        ]
    },

    'seller': {
        'menu': '',
        'heading': [
            'list',
            'add',
            'edit',
            'view',
        ],
        'list': [
            'delete',
            'add',
            'deleted',
            'bulkDeleted',
            'confirmDelete',
            'confirmBulkDelete',
            'edited',
            'added',
            'notFound',
            'noRecords',
        ]
    }
}

DEFAULT_FIELDS = {
    'success': '',
    'hint': '',
    'errors': {
        'maxLength': _('Exceeded max number of characters'),
        'required': _('Field is required'),
        'incorrect': _('Value is incorrect'),
    }
}

DYNAMIC_ENTRIES = {
    'category': {
        'name': Category._meta.verbose_name,
        'name_plural': Category._meta.verbose_name_plural,
        'fields': {
            'name': {
                'label': Category._meta.get_field('name').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': DEFAULT_FIELDS['errors'],
            },
            'parent': {
                'label': Category._meta.get_field('parent').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            }
        }
    },
    'item': {
        'name': Item._meta.verbose_name,
        'name_plural': Item._meta.verbose_name_plural,
        'fields': {
            'category': {
                'label': Item._meta.get_field('category').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
            'sku': {
                'label': Item._meta.get_field('sku').verbose_name,
            }
        }
    },
    'seller': {
        'name': _('seller'),
        'name_plural': _('sellers'),
        'fields': {
            'first_name': {
                'label': User._meta.get_field('first_name').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
            'last_name': {
                'label': User._meta.get_field('last_name').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
            'phone': {
                'label': User._meta.get_field('phone').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
            'store': {
                'label': Store._meta.verbose_name,
            },
            'full_name': {
                'label': _('full_name'),
            },
        }
    },
    'store': {
        'name': Store._meta.verbose_name,
        'name_plural': Store._meta.verbose_name_plural,
        'fields': {
            'address': {
                'label': Store._meta.get_field('address').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
            'seller': {
                'label': Store._meta.get_field('seller').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
        }
    },
    'login': {
        'fields': {
            'username': {
                'label': User._meta.get_field(User.USERNAME_FIELD).verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
            'password': {
                'label': User._meta.get_field('password').verbose_name,
                'success': DEFAULT_FIELDS['success'],
                'hint': DEFAULT_FIELDS['hint'],
                'errors': {
                    'required': DEFAULT_FIELDS['errors']['required'],
                    'incorrect': DEFAULT_FIELDS['errors']['incorrect']
                },
            },
        }
    }
}
