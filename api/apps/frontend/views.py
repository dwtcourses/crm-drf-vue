import importlib
import os

from django.conf import settings
from django.http import HttpResponseNotFound
from django.shortcuts import render
from django.utils.translation import get_language
from django.utils.translation import gettext as _
from django.views.i18n import DjangoTranslation, JSONCatalog

from rest_framework import authentication, exceptions, permissions, views

from apps.api.components.data import InputsData
from apps.api.views import JSONI18n


# Create your views here.
class Initial(views.APIView):
    """
    Initial data

    """
    authentication_classes = []
    permission_classes = [permissions.AllowAny]

    def get(self, request, **kwargs):
        try:
            user, token = authentication.TokenAuthentication().authenticate(request)
            request.user = user
        except Exception:
            pass

        context = {
            'locale': get_language(),
            'inputsData': InputsData(),
            'i18n': JSONI18n().get_context_data(
                **{'domain': 'i18n', 'locale': get_language(), 'app': 'apps.frontend'}),
            'user': request.user.serializer.data if request.user.is_authenticated else None
        }

        return views.Response(context)
