Demo CRM application.
Django as REST API and Vuejs as frontend

Installation:
Run init.sh bash script in root directory


Demo: [here](http://demo-crm.denis-ilchishin.pp.ua)

Login demo creadentials:
* Phone: 99 999 9999 (just fill the field with 9's)
* Pass: demo
