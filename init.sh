#!/bin/bash
cd "${0%/*}"

read -p "Python entreperter: " PYTHON_ENTR
read -p "API domain: " API_DOMAIN
read -p "Frontend domain: " FRONTEND_DOMAIN
read -p "Mysql host: " MYSQL_HOST
read -p "Mysql database name: " MYSQL_DB
read -p "Mysql username: " MYSQL_USER
read -sp "Mysql password: " MYSQL_PASS

eval "$PYTHON_ENTR -m pip install virtualenv"
eval "$PYTHON_ENTR -m venv api/.pyenv"

eval "cd api/ && .pyenv/bin/pip install -r requirements.txt"

LOCAL_PY_SETTINGS="
SECRET_KEY = 'SUPER_SUPER_SECRET_KEY_MUST_BE_CHACGED'
DEBUG = False
ALLOWED_HOSTS = [
  '$API_DOMAIN'
]
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = [
  'http://$FRONTEND_DOMAIN',
  'https://$FRONTEND_DOMAIN',
]

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {  # MUST BE overridden in settings/local.py
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '$MYSQL_DB',
        'HOST': '$MYSQL_HOST',
        'USER': '$MYSQL_USER',
        'PASSWORD': '$MYSQL_PASS',
    }
}
"
LOCAL_PY_SETTINGS_PATH="crm/settings/local.py"
echo "$LOCAL_PY_SETTINGS" > "$LOCAL_PY_SETTINGS_PATH"


eval ".pyenv/bin/python manage.py migrate"
eval ".pyenv/bin/python manage.py compilemessages"

cd ../frontend

VUEJS_ENV="
VUE_APP_FRONTEND_ENDPOINT=http://$API_DOMAIN/frontend/
VUE_APP_API_ENDPOINT=http://$API_DOMAIN/api/
"

ENV_PATH=".env.production.local"
echo "$VUEJS_ENV" > "$ENV_PATH"

npm install
npm run build

echo "
Now you can create new user using \"manage.py createsuperuser\" command.
P.S phone number must be \"380999999999\" format (Ukrainian phone number format), where \"9\" is any digit
You can change that in \"api/apps/api/components/utils.py\" phone_validator regex rule
"
